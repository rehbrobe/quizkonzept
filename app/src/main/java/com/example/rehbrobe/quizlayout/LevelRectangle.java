package com.example.rehbrobe.quizlayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by reitech on 19.12.17.
 */

public class LevelRectangle extends View {

    private final Paint paint;


    public LevelRectangle(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        //Circle color
        paint.setColor(Color.WHITE);

    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float dpsWidth = 40;
        float pxsWidth = dpsWidth * getResources().getDisplayMetrics().density;

        float dpsHeight = 15;
        float pxsHeight = dpsHeight * getResources().getDisplayMetrics().density;

        float dpsLeft = 0; // basically (X1, Y1)


        float dpsTop = 0; // basically (X1, Y1)


        float right = dpsLeft + pxsWidth; // width (distance from X1 to X2)
        float bottom = dpsTop + pxsHeight; // height (distance from Y1 to Y2)


        RectF rectangle = new RectF(dpsLeft, dpsTop, right, bottom);
        canvas.drawRect(rectangle, paint);
    }


}
