package com.example.rehbrobe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

/**
 * Created by reitech on 11.01.18.
 */

public class KnowledgebaseSearchAdapter extends BaseAdapter implements Filterable {

    private Context context;
    JSONArray topicJSONArray;
    JSONArray filteredTopicsArray;
    JSONArray topicData;
    TopicFilter topicFilter;
    boolean searchModus;


    public KnowledgebaseSearchAdapter(Context context, JSONArray topicJSONArray) {
        this.context = context;
        this.topicJSONArray = topicJSONArray;
        this.filteredTopicsArray = new JSONArray();
        this.topicData = topicJSONArray;


    }

    public View getView(int position, View convertView, ViewGroup parent) {



        try {

            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);




                    convertView = inflater.inflate(R.layout.knowledgebase_grid_searchresult, null);

            }

            //get topic data
            JSONObject topic = topicJSONArray.getJSONObject(position);


                String imageID = topic.getString("image");
                ImageView thumb = (ImageView) convertView.findViewById(R.id.search_image);
                thumb.setImageResource(context.getResources().getIdentifier(imageID, "drawable", context.getPackageName()));
                TextView searchTextHeading = (TextView) convertView.findViewById(R.id.topic_label);
                TextView searchTextContent = (TextView) convertView.findViewById(R.id.search_topic_text);

                searchTextHeading.setText(topic.getString("heading"));
                searchTextContent.setText(html2text(topic.getString("content")));






        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return convertView;
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    public void updateGridView(JSONArray topicJSONArray){

        this.topicJSONArray = topicJSONArray;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return topicJSONArray.length();
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public Filter getFilter() {
        if (topicFilter == null) {
            topicFilter = new TopicFilter();
        }
        return topicFilter;
    }

    public void setSearchModus(boolean modus){
        searchModus = modus;
    }


    private class TopicFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence searchText) {
            filteredTopicsArray = new JSONArray();
            searchText = searchText.toString().toLowerCase();
            FilterResults results = new FilterResults();

            if(searchText != null && searchText.length() > 0){



                try {
                    for (int i = 0; i < topicJSONArray.length(); i++) {

                        JSONObject topic = topicJSONArray.getJSONObject(i);

                        if(topic.getString("heading").toLowerCase().contains(searchText) ||
                                topic.getString("content").toLowerCase().contains(searchText)){

                            System.out.println("found in heading / content");
                            filteredTopicsArray.put(topic);


                        }

                            //check subtopics too
                            JSONArray subTopics = topic.getJSONArray("subheadings");

                            for(int j = 0; j < subTopics.length(); j++){

                                JSONObject subTopic = subTopics.getJSONObject(j);

                                if(subTopic.getString("heading").toLowerCase().contains(searchText) ||
                                        subTopic.getString("content").toLowerCase().contains(searchText)){
                                    System.out.println("found in subtopics heading / content");
                                    subTopic.put("image", topic.get("image"));
                                    filteredTopicsArray.put(subTopic);


                                }
                            }

                    }

                    results.count = filteredTopicsArray.length();
                    results.values = filteredTopicsArray;
                }
                catch(JSONException e){

                }

            }else{
                results.count = topicData.length();
                results.values = topicData;
            }


            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            topicJSONArray = (JSONArray) filterResults.values;
            KnowledgebaseSearchAdapter.this.notifyDataSetChanged();
            notifyDataSetInvalidated();
        }
    }
}
