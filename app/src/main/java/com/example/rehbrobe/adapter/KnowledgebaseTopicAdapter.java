package com.example.rehbrobe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by reitech on 11.01.18.
 */

public class KnowledgebaseTopicAdapter extends BaseAdapter {

    private Context context;
    JSONArray topicJSONArray;
    JSONArray filteredTopicsArray;
    JSONArray topicData;
    boolean searchModus;

    public Integer[] imageIDs = {R.drawable.virus, R.drawable.anst, R.drawable.condoms, R.drawable.microscope, R.drawable.aidslife};


    public KnowledgebaseTopicAdapter(Context context, JSONArray topicJSONArray) {
        this.context = context;
        this.topicJSONArray = topicJSONArray;
        this.filteredTopicsArray = new JSONArray();
        this.topicData = topicJSONArray;


    }

    public View getView(int position, View convertView, ViewGroup parent) {



        try {

            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    // get layout
                    convertView = inflater.inflate(R.layout.kb_grid_topic_v2, null);

            }

            //get topic data
            JSONObject topic = topicJSONArray.getJSONObject(position);

            System.out.println(position);

                // set value into textview
                TextView textView = (TextView) convertView
                        .findViewById(R.id.topic_label);

                textView.setText(topic.getString("heading"));

                // set image based on selected text
                ImageView imageView = (ImageView) convertView
                        .findViewById(R.id.topic_image);


                imageView.setImageResource(imageIDs[position]);






        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return convertView;
    }

    public void updateGridView(JSONArray topicJSONArray){

        this.topicJSONArray = topicJSONArray;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return topicJSONArray.length();
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
