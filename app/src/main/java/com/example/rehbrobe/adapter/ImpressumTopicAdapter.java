package com.example.rehbrobe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by reitech on 16.03.18.
 */

public class ImpressumTopicAdapter extends BaseAdapter {

    private Context context;
    String [] titles = {"Beratung & Test","Du hast eine Frage?","Impressum"};
    //String [] titles = {"Beratung","Teste dich","Du hast eine Frage?","Impressum"};

    String [] texts = {"","",""};
    //String [] texts = {"Lass dich von uns beraten","Mehr Informationen über unsere Tests","Tritt mit uns direkt in Kontakt","AIDS-Hilfe Steiermark"};

    Integer [] imageIDs = {R.drawable.kondom_02,R.drawable.kondom_03,R.drawable.kondom_04,R.drawable.kondom_07};

    public ImpressumTopicAdapter(Context context){

        this.context = context;

    }

    public View getView(int position, View convertView, ViewGroup parent) {


            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                // get layout
                convertView = inflater.inflate(R.layout.impressum_table_row, null);

            }

        // set image based on selected text
        ImageView imageView = (ImageView) convertView
                .findViewById(R.id.topic_image);
            imageView.setImageResource(R.drawable.redribbon);

            //set label text
        TextView title = (TextView) convertView.findViewById(R.id.topic_label);
        title.setText(titles[position]);

        //set label text
        TextView text = (TextView) convertView.findViewById(R.id.topic_text);
        text.setText(texts[position]);




        return convertView;
    }


    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}

