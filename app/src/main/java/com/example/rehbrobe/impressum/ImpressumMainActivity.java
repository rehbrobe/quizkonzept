package com.example.rehbrobe.impressum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.rehbrobe.adapter.ImpressumTopicAdapter;
import com.example.rehbrobe.knowledgebase.KnowledgebaseMainActivity;
import com.example.rehbrobe.quizkonzept.QuizDashboard;
import com.example.rehbrobe.quizkonzept.R;

public class ImpressumMainActivity extends Activity
{

    GridView topicGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.impressum_overview);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        Menu menu =  navigation.getMenu();
        MenuItem item = menu.getItem(2);
        item.setChecked(true);

        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
        {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.navigationBar_LeftItem:
                    {
                        Intent showDashboard = new Intent(ImpressumMainActivity.this, QuizDashboard.class);
                        startActivity(showDashboard);
                        break;
                    }


                    case R.id.navigationBar_CenterItem:
                    {
                        Intent showKB = new Intent(ImpressumMainActivity.this, KnowledgebaseMainActivity.class);
                        startActivity(showKB);
                        break;
                    }

                    case R.id.navigationBar_RightItem:
                    {
                        //setContentView(R.layout.impressum_overview);
                        //Intent showAbout = new Intent(QuizDashboard.this, ImpressumMainActivity.class);
                        //startActivity(showAbout);
                        break;
                    }
                }
                return false;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        topicGrid = (GridView) findViewById(R.id.topic_grid);
        final ImpressumTopicAdapter topicAdapter = new ImpressumTopicAdapter(this);
        topicGrid.setAdapter(topicAdapter);

        //set click listener
        topicGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView textView = (TextView) view
                        .findViewById(R.id.topic_label);

                String topicName = textView.getText().toString();
                if(topicName.contains("Frage")){

                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("plain/text");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "steirische@aids-hilfe.at" });
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Frage");
                    intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                    startActivity(Intent.createChooser(intent, ""));
                }else {
                    //create Intent and pass topic data
                    Intent impressumTopic = new Intent(getApplicationContext(), ImpressumTopic.class);
                    impressumTopic.putExtra("topicName", topicName);
                    startActivity(impressumTopic);
                }
            }

        });

    }
}
