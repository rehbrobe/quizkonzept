package com.example.rehbrobe.impressum;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.widget.ImageView;

import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by reitech on 16.03.18.
 */

public class ImpressumTopic   extends Activity{


    WebView txtView;
    ImageView imgView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.knowledgebase_topic);

        txtView = (WebView) findViewById(R.id.topicContent);
        imgView = (ImageView) findViewById(R.id.topicimage);

        Bundle extras = getIntent().getExtras();
        String topicContent = "";
        Integer imageID = null;

        String topicName = extras.getString("topicName");

        switch (topicName){

            case "Beratung & Test": {
                imageID = R.drawable.beratung_heading;
                topicContent = createBeratungHTMLContent();
                break;
            }
            /*case "Teste dich":{
                imageID = R.drawable.testing_heading;
                topicContent = createTestHTMLContent();
                break;
            }*/
            case "Du hast eine Frage?":{
                imageID = R.drawable.kondom_04;
                break;
            }
            case "Impressum":{
                imageID = R.drawable.ahs_logo;
                topicContent = createImpressumHTMLContent();
                break;
            }
        }

        imgView.setImageResource(imageID);
        txtView.loadDataWithBaseURL("app:htmlPage24",topicContent,"text/html","UTF-8", null);

    }

    private String createBeratungHTMLContent() {

        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body style=\"padding: 20px;\">\n";
//        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Persönliche Beratung" + "</h2>";
//        html = html + "<p>" + "Vor der Blutabnahme findet ein Vier-Augen-Gespräch statt, " +
//                "in dem persönliche Fragen und Anliegen besprochen und diskutiert werden können. " +
//                "Das Testergebnis wird nur persönlich übermittelt, nicht am Telefon und auch nicht " +
//                "einer anderen Person. Das Beratungsgespräch vor der Blutabnahme und die Befundrückgabe kann " +
//                "man auch gemeinsam mit z.B. dem Partner/der Partnerin machen.<br><br>"+
//                "Da es sich um eine anonyme Testung handelt, werden keinerlei schriftliche Bestätigungen ausgehändigt." + "<br><br>" +
//                "Beratung und Test sind anonym. <br>" +
//                "Sie erhalten am Empfang eine Nummer, mit der Sie aufgerufen werden. <br>" +
//                "Die Nummernausgabe ist begrenzt  und startet 15 Minuten vor den Test- und Beratungszeiten. </p>";
//
//        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Beratungszeiten</h3>";
//        html = html + "<p>" + "<b>Di, Mi & Do:<br>" +
//                "16:30 - 19:30 Uhr<br><br>" +
//                "Fr:<br>" +
//                "17:00 - 19:00 Uhr</b>" + "</p>";
//
//        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Beratung per Email</h3>";
//        html = html + "<p>" + "<b>beratung@aids-hilfe.at</b>" + "</p>";
//
//        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Telefonische Beratung</h3>";
//        html = html + "<p>" +
//                "<b>(+43) 0316/81 50 50</b>" + "</p>";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Annonym und Kostenlos" + "</h2>";
        html = html + "<p>" + "In der AIDS-Hilfe Steiermark kannst Du Dich kostenlos und anonym zu Fragen über HIV/ AIDS und sexuell übertragbare Infektionen beraten lassen. " +
                "Lorem " +
                "Ipsum.<br><br>"+"</p>";


        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Der HIV-Test" + "</h2>";
        html = html + "<p>" + "Mit einem HIV-Test kann man nachweisen, ob man sich bei einem Risiko mit HIV-Infiziert hat. " +
                "Im Falle einer Infektion bildet das menschlische Immunsystem Antikörper. " +
                "Dieser Prozess der Antikörperbildung ist individuell verschieden und kann bis zu 6 Wochen dauern. " +
                "Im Test werden diese Antikörper gesucht.<br><br>"+"</p>";

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Testzeiten</h3>";
        html = html + "<p>" + "<b>Di, Mi & Do:<br>" +
                "16:30 - 19:00 Uhr</b>"+ "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Ort</h3>";
        html = html + "<p>" + "<b>Hans-Sachs-Gasse 3,<br>" +
                "1. Stock<br>" +
                "8010 Graz</b><br><br>" + "</p>";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Testarten" + "</h2>";
        html = html + "<p>" +  "Folgende Tests bieten wir in der AIDS-Hilfe Steiermark an:" + "" +
                "<ul>" +
                "<li>HIV-Test, anonym und kostenlos (Ergebnis nach 3 bis 5 Tagen)</li><br>" +
                "<li>HIV-Schnelltest - Ergebnis 30 Minuten nach der Blutabnahme, anonym, € 28,-- (Annahme bis spätestens 18:00 Uhr)</li><br>"+
                "<li>Syphilis-Test, anonym,  € 18,--</li>"+
                "</ul>" +"</p>";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Sicheres Testergebnis" + "</h2>";
        html = html + "<p>" + "Man kann schon ab der dritten Woche nach einem Risiko einen Test durchführen lassen. " +
                "Dieses Ergebnis ist allerdings nur ein vorläufiges und muss 6 Wochen nach dem Risiko mit einer neuen Blutprobe " +
                "bestätigt werden.<br><br>" +
                "Wenn 6 Wochen nach einer Risikosituation im Blut keine Antikörper gefunden werden, " +
                "ist das Testergebnis negativ, d.h. man hat sich bei diesem Risiko nicht mit HIV infiziert.<br><br>" +
                "Aufgrund der Anonymität stellt die AIDS-Hilfe Steiermark keine schriftlichen Befunde aus, auch eine Voranmeldung ist nicht möglich. E-Card oder Ausweis ist nicht nötig. Also einfach vorbeikommen! " + "</p>";


        return html;
    }

    private String createTestHTMLContent() {

        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body style=\"padding: 20px;\">\n";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Annonym und Kostenlos" + "</h2>";
        html = html + "<p>" + "Lorem Ipmsum " +
                "Lorem " +
                "Lorem " +
                "Ipsum.<br><br>"+"</p>";


        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Der HIV-Test" + "</h2>";
        html = html + "<p>" + "Mit einem HIV-Test kann man nachweisen, ob man sich bei einem Risiko mit HIV-Infiziert hat. " +
                "Im Falle einer Infektion bildet das menschlische Immunsystem Antikörper. " +
                "Dieser Prozess der Antikörperbildung ist individuell verschieden und kann bis zu 6 Wochen dauern. " +
                "Im Test werden diese Antikörper gesucht.<br><br>"+"</p>";

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Testzeiten</h3>";
        html = html + "<p>" + "<b>Di, Mi & Do:<br>" +
                "16:30 - 19:00 Uhr</b>"+ "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Ort</h3>";
        html = html + "<p>" + "<b>Hans-Sachs-Gasse 3,<br>" +
                "1. Stock<br>" +
                "8010 Graz</b><br><br>" + "</p>";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Testarten" + "</h2>";
        html = html + "<p>" +  "Folgende Tests bieten wir in der AIDS-Hilfe Steiermark an:" + "" +
                "<ul>" +
                "<li>HIV-Test, anonym und kostenlos (Ergebnis nach 3 bis 5 Tagen)</li><br>" +
                "<li>HIV-Schnelltest - Ergebnis 30 Minuten nach der Blutabnahme, anonym, € 28,-- (Annahme bis spätestens 18:00 Uhr)</li><br>"+
                "<li>Syphilis-Test, anonym,  € 18,--</li>"+
                "</ul>" +
                "Beratung und Test sind anonym. <br>" +
                "Sie erhalten am Empfang eine Nummer, mit der Sie aufgerufen werden. <br>" +
                "Die Nummernausgabe ist begrenzt  und startet 15 Minuten vor den Test- und Beratungszeiten. </p>";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + "Sicheres Testergebnis" + "</h2>";
        html = html + "<p>" + "Man kann schon ab der dritten Woche nach einem Risiko einen Test durchführen lassen. " +
                "Dieses Ergebnis ist allerdings nur ein vorläufiges und muss 6 Wochen nach dem Risiko mit einer neuen Blutprobe " +
                "bestätigt werden.<br><br>" +
                "Wenn 6 Wochen nach einer Risikosituation im Blut keine Antikörper gefunden werden, " +
                "ist das Testergebnis negativ, d.h. man hat sich bei diesem Risiko nicht mit HIV infiziert.<br><br>" +
                "Aufgrund der Anonymität stellt die AIDS-Hilfe Steiermark keine schriftlichen Befunde aus, auch eine Voranmeldung ist nicht möglich. E-Card oder Ausweis ist nicht nötig. Also einfach vorbeikommen! " + "</p>";



        return html;
    }



    private String createImpressumHTMLContent() {

        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body style=\"padding: 20px;\">\n";
        html = html + "<h3 style=\"color: #e2494c;\" id=\"topic\">" + "Adresse" + "</h3>";
        html = html + "<p>" + "<b>Hans-Sachs-Gasse 3<br>" +
                "8010 Graz<br>" +
                "steirische@aids-hilfe.at<br>" +
                "Tel: (+43) 0316 81 50 50<br>" +
                "Fax: (+43) 0316 81 50 50 6<br>" +
                "ZVR-Zahl 456426977<br>" +
                "Website der AIDS-Hilfe Steiermark:<br>" +
                "<a href=\"https://www.aids-hilfe.at/\">Webseite</a><br><br>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">Beratungszeiten</h3>";
        html = html + "<p>" + "<b>Di, Mi & Do:<br>" +
                "16:30 - 19:30 Uhr<br><br>" +
                "Fr:<br>" +
                "17:00 - 19:00 Uhr</b>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Beratung per Email</h3>";
        html = html + "<p>" + "<b>beratung@aids-hilfe.at</b>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Telefonische Beratung</h3>";
        html = html + "<p>" +
                "<b>(+43) 0316/81 50 50</b>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Testzeiten</h3>";
        html = html + "<p>" + "<b>Di, Mi & Do:<br>" +
                "16:30 - 19:00 Uhr</b>"+ "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Ort</h3>";
        html = html + "<p>" + "<b>Hans-Sachs-Gasse 3,<br>" +
                "1. Stock<br>" +
                "8010 Graz</b><br><br>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Entwicklung & Design</h3>";
        html = html + "<p>" + "<b>Institut eHealth</b><br>" +
                "FH-JOANNEUM<br>" +
                "<a href=\"http://fh-joanneum.at/ieh\">Webseite</a><br><br>" + "</p>";

        html = html + "<h3 style=\"color: #e2494c;\" >Bildnachweis</h3>";
        html = html + "<p>" + "<b>AIDS-Hilfe Steiermark<br>" +
                "Institut for Web Science and Technologies, Univerity of Koblenz-Landau<br>" +
                "</p>";


        return html;
    }
}
