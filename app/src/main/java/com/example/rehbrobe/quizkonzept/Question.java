package com.example.rehbrobe.quizkonzept;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rehbrobe.quizlayout.LevelRectangle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class Question extends Activity implements View.OnClickListener
{

    Button btNext;

    CheckBox chBox1, chBox2, chBox3, chBox4;

    TextView tvTitle, tvCategorie, tvQuestionNr;

    ImageView person;

    int score, questionNr, difficulty=1, topicID=0, number;

    String actCategoryName;

    static ArrayList<String> topicsToLearn;

    JSONArray questionsOfActTopic = null;
    JSONObject actQ = null;

    public int seconds = 0;
    public int minutes = 0;

    SharedPreferences mySPR;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quzi_question);

        person = (ImageView) findViewById(R.id.personImg);
        btNext = (Button)findViewById(R.id.btNext);

        chBox1 = (CheckBox)findViewById(R.id.checkBox1);
        chBox2 = (CheckBox)findViewById(R.id.checkBox2);
        chBox3 = (CheckBox)findViewById(R.id.checkBox3);
        chBox4 = (CheckBox)findViewById(R.id.checkBox4);

        tvTitle = (TextView)findViewById(R.id.tVTitle);

        tvCategorie = (TextView)findViewById(R.id.id_categorie);
        tvQuestionNr = (TextView)findViewById(R.id.id_questionNr);

        btNext.setOnClickListener(this);

        mySPR = getSharedPreferences("mySPfile",MODE_PRIVATE);

        score=0;
        questionNr=-1;
        number=0;

        topicsToLearn = new ArrayList<>();

        Bundle extras = getIntent().getExtras();

        if(extras != null)
        {
            topicID = extras.getInt("selectedRow") / 3;

            actCategoryName = extras.getString("selectedCategoryName");
            tvCategorie.setText(actCategoryName);

            int tmp = extras.getInt("selectedRow") + 1;

            System.out.println("selected Row +1: " + tmp);

            if((tmp%3)==1)
            {
                difficulty = 1;
                LevelRectangle rc = (LevelRectangle) findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            if((tmp%3)==2)
            {
                difficulty = 2;
                LevelRectangle rc = (LevelRectangle) findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) findViewById(R.id.rec2);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            if((tmp%3)==0)
            {
                difficulty = 3;
                LevelRectangle rc = (LevelRectangle) findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) findViewById(R.id.rec2);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) findViewById(R.id.rec3);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }

        System.out.println("Topic: " + topicID + "= " + actCategoryName);
        System.out.println("Difficulty: " + difficulty);

        loadQuestionsOfTopic(topicID);


        //Declare the timer
        Timer t = new Timer();
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TextView tv = (TextView) findViewById(R.id.id_question_time);
                        if(seconds < 10)
                        {
                            tv.setText(String.valueOf(minutes)+":0"+String.valueOf(seconds));
                        }else
                        {
                            tv.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        }
                        if(seconds == 59)
                        {
                            tv.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));

                            seconds=0;
                            minutes=minutes+1;
                        }else
                        {
                            seconds = seconds + 1;
                        }
                    }
                });
            }
        }, 0, 1000);

    }

    private void animateViews(Integer imageID){

        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        anim.setRepeatCount(0);
        anim.setRepeatMode(Animation.RESTART);

        AlphaAnimation anim2 = new AlphaAnimation(0.0f, 1.0f);
        anim2.setDuration(2000);
        anim2.setRepeatCount(0);
        anim2.setRepeatMode(Animation.RESTART);

        person.setImageResource(imageID);

        tvTitle.setAnimation(anim);
        chBox1.setAnimation(anim2);
        chBox2.setAnimation(anim2);
        chBox3.setAnimation(anim2);
        chBox4.setAnimation(anim2);

    }

    private String loadJSONFromAsset()
    {
        String json;

        try
        {
            InputStream is = getAssets().open("questions.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void loadQuestionsOfTopic(int topicID)
    {
        try
        {
            JSONObject myJsonObj = new JSONObject(loadJSONFromAsset());

            System.out.println(myJsonObj);

            JSONArray topics = myJsonObj.getJSONArray("topics");

            JSONObject actTopic = topics.getJSONObject(topicID);

            questionsOfActTopic = actTopic.getJSONArray("questions");

            System.out.println("loaded");

            displayNextQuestion();

        }
        catch (JSONException e)
        {
            System.out.println("Error while loading questions form JSON: " + e.getMessage());
        }
    }

    private void displayNextQuestion()
    {
        questionNr++;



        try
        {
            if(questionNr < questionsOfActTopic.length())
            {
                if(questionNr % 2 == 1) {
                    animateViews(R.drawable.men_s);
                }else{
                    animateViews(R.drawable.woman_s);
                }

                actQ = questionsOfActTopic.getJSONObject(questionNr);

                if(actQ.getInt("type") == difficulty)
                {
                    number++;
                    tvQuestionNr.setText(number + " / 7");

                    JSONArray options = actQ.getJSONArray("options");
                    tvTitle.setText(actQ.getString("qText"));

                    int randomNr = (int) (Math.random() * (4 - 1) + 1);

                    switch (randomNr) {
                        case 1:
                            chBox1.setText(options.getString(0));
                            chBox2.setText(options.getString(1));
                            chBox3.setText(options.getString(2));
                            chBox4.setText(options.getString(3));
                            break;
                        case 2:
                            chBox1.setText(options.getString(1));
                            chBox2.setText(options.getString(2));
                            chBox3.setText(options.getString(3));
                            chBox4.setText(options.getString(0));
                            break;
                        case 3:
                            chBox1.setText(options.getString(2));
                            chBox2.setText(options.getString(3));
                            chBox3.setText(options.getString(0));
                            chBox4.setText(options.getString(1));
                            break;
                        case 4:
                            chBox1.setText(options.getString(3));
                            chBox2.setText(options.getString(0));
                            chBox3.setText(options.getString(1));
                            chBox4.setText(options.getString(2));
                            break;
                    }
                }
                else
                {
                    displayNextQuestion();
                }
            }
            else
            {
                writeToSP();

                Intent finishGameIntent = new Intent(this, Results.class);
                finishGameIntent.putExtra("score", score);
                finishGameIntent.putExtra("minutes", minutes);
                finishGameIntent.putExtra("seconds", seconds);
                finishGameIntent.putExtra("category", actCategoryName);
                finishGameIntent.putExtra("difficulty", difficulty);
                startActivity(finishGameIntent);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void writeToSP()
    {
        switch (difficulty)
        {
            case 1:
            {
                if(mySPR.getInt(actCategoryName + "hs1",0) < score){
                    System.out.println("Neue HighScore:" + Integer.toString(score));
                    editor = mySPR.edit();
                    editor.putInt(actCategoryName + "hs1",score);
                    editor.commit();
                }else
                {
                    System.out.println("Deine Score:" + Integer.toString(score));
                }
                break;
            }
            case 2:
            {
                if(mySPR.getInt(actCategoryName + "hs2",0) < score){
                    System.out.println("Neue HighScore:" + Integer.toString(score));
                    editor = mySPR.edit();
                    editor.putInt(actCategoryName + "hs2",score);
                    editor.commit();
                }else
                {
                    System.out.println("Deine Score:" + Integer.toString(score));
                }
                break;
            }
            case 3:
            {
                if(mySPR.getInt(actCategoryName + "hs3",0) < score){
                    System.out.println("Neue HighScore:" + Integer.toString(score));
                    editor = mySPR.edit();
                    editor.putInt(actCategoryName + "hs3",score);
                    editor.commit();
                }else
                {
                    System.out.println("Deine Score:" + Integer.toString(score));
                }
                break;
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        ArrayList<String> givenAnswers = new ArrayList<String>();

        if(chBox1.isChecked())
        {
                givenAnswers.add(chBox1.getText().toString());
        }
        System.out.println(givenAnswers);

        if(chBox2.isChecked())
        {
            givenAnswers.add(chBox2.getText().toString());
        }
        System.out.println(givenAnswers);

        if(chBox3.isChecked())
        {
            givenAnswers.add(chBox3.getText().toString());
        }
        System.out.println(givenAnswers);

        if(chBox4.isChecked())
        {
            givenAnswers.add(chBox4.getText().toString());
        }
        System.out.println(givenAnswers);

        chBox1.setChecked(false);
        chBox2.setChecked(false);
        chBox3.setChecked(false);
        chBox4.setChecked(false);

        evaluateAnswer(givenAnswers);
    }

    private void evaluateAnswer(ArrayList<String> givenAnswers)
    {
        int numberOfCorrectOptions;
        try
        {
            JSONArray correctAnswers = actQ.getJSONArray("correctAnswers");

            System.out.println("Anz givenAnsw: " + givenAnswers.size());

            if(givenAnswers == null && correctAnswers.length()==0)
            {
                score = score + difficulty;

                int tmp = mySPR.getInt("correctQuSum", 0);
                tmp = tmp +1;

                editor = mySPR.edit();
                editor.putInt("correctQuSum",tmp);
                editor.commit();

                displayNextQuestion();
            }
            if(givenAnswers != null && correctAnswers.length()==0)
            {
                int tmp = mySPR.getInt("incorrectQuSum", 0);
                tmp = tmp +1;

                editor = mySPR.edit();
                editor.putInt("incorrectQuSum",tmp);
                editor.commit();

                if(!topicsToLearn.contains(actQ.getString("keyWord")))
                {
                    topicsToLearn.add(actQ.getString("keyWord"));
                }

                displayNextQuestion();
            }
            else
            {
                numberOfCorrectOptions = correctAnswers.length();

                for(int i =0; i < givenAnswers.size() ; i++)
                {
                    for(int j = 0; j < correctAnswers.length(); j++)
                    {
                        System.out.println("givenanswer: " + givenAnswers.get(i));
                        System.out.println("correct answer: " + correctAnswers.get(j));

                        if(givenAnswers.get(i).equals(correctAnswers.get(j)))
                        {
                            System.out.println("veringern");
                            numberOfCorrectOptions--;
                        }
                    }
                }

                System.out.println("remaining Corrct answers: " + numberOfCorrectOptions);
                if(numberOfCorrectOptions==0)
                {
                    score = score + difficulty;

                    int tmp = mySPR.getInt("correctQuSum", 0);
                    tmp = tmp +1;

                    editor = mySPR.edit();
                    editor.putInt("correctQuSum",tmp);
                    editor.commit();

                    displayNextQuestion();
                }
                else
                {
                    int tmp = mySPR.getInt("incorrectQuSum", 0);
                    tmp = tmp +1;

                    editor = mySPR.edit();
                    editor.putInt("incorrectQuSum",tmp);
                    editor.commit();

                    if(!topicsToLearn.contains(actQ.getString("keyWord")))
                    {
                        topicsToLearn.add(actQ.getString("keyWord"));
                    }

                    displayNextQuestion();
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}