package com.example.rehbrobe.quizkonzept;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rehbrobe.quizlayout.LevelRectangle;

public class Results extends Activity implements View.OnClickListener
{
    Button btnEndGame;
    TextView tVScore, tVTime, tVPercentage;
    ListView resultList;

    int minutes, seconds, difficulty, score = 0;

    String actCatName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quzi_result);

        tVScore = (TextView) findViewById(R.id.tVScore);
        tVTime = (TextView) findViewById(R.id.id_questions_time);
        tVPercentage = (TextView) findViewById(R.id.id_question_score);

        Bundle extras = getIntent().getExtras();

        if(extras != null)
        {
            System.out.println("Übergebene Score:" + extras.getInt("score"));

            minutes = extras.getInt("minutes");
            seconds = extras.getInt("seconds");

            difficulty = extras.getInt("difficulty");
            System.out.println("Difficulty: "+difficulty);

            score = extras.getInt("score");
            System.out.println("Score: "+score);

            actCatName = extras.getString("category");
        }

        resultList = (ListView) findViewById(R.id.resultList);

        showResults();
    }

    private void showResults()
    {
        tVScore.setText(score + "/" + (7*difficulty));
        tVTime.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));

        float percentage = ((float)score / (float)(7*difficulty)) * 100;

        startCountAnimation(0.0f, percentage);

        resultList.setAdapter(new Results.CategoryListAdapter());
    }

    private void startCountAnimation(float start, float end)
    {
        ValueAnimator animator = ValueAnimator.ofFloat(start, end);
        animator.setDuration(2000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                tVPercentage.setText(String.format("%.1f",animation.getAnimatedValue())+"%");
            }
        });
        animator.start();
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            /*
            case R.id.btnTryAgain:
            {
                Intent startGameIntent = new Intent(this, Question.class);
                startActivity(startGameIntent);
                break;
            }*/
            case R.id.btnDashboard:
            {
                Intent goToHomeScreen = new Intent(this, QuizDashboard.class);
                startActivity(goToHomeScreen);
                break;
            }
        }
    }

    private class CategoryListAdapter extends BaseAdapter
    {
        private LayoutInflater mInflater;

        public CategoryListAdapter()
        {
            mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public Object getItem(int i)
        {
            return i;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            switch (i)
            {
                case 0:
                {
                    view = initCategoryView(view);
                    break;
                }
                case 1:
                {
                    view = initResultView(view);
                    break;
                }
            }
            return view;
        }

        private View initCategoryView(View view)
        {
            view = mInflater.inflate(R.layout.category_row, null);

            view.findViewById(R.id.id_catRowBackG).setBackgroundColor(Color.TRANSPARENT);
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorDarkGreen));
            view.findViewById(R.id.id_btn_play).setEnabled(false);

            if(levelCompleted())
            {
                view.findViewById(R.id.id_btn_play).setBackgroundResource(R.drawable.ic_action_completed);
            }else
            {
                view.findViewById(R.id.id_btn_play).setBackgroundResource(R.drawable.ic_action_redo);
            }

            TextView tvCat = (TextView) view.findViewById(R.id.id_cat_name);
            tvCat.setText(actCatName);

            TextView tvScore = (TextView) view.findViewById(R.id.id_cat_scorePoints);
            tvScore.setText(String.valueOf(score));

            TextView tvCategoryDescription = (TextView) view.findViewById(R.id.id_cat_desc);

            TextView tvScoreInPercent = (TextView) view.findViewById(R.id.id_cat_scorePercentage);

            switch(difficulty){

                case 1:
                {
                    tvScoreInPercent.setText(((score*100)/7) + "%");

                    tvCategoryDescription.setText("In dieser Kategorie werden dir einfache Fragen zum jeweiligen Thema gestellt.");

                    //view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorCatGreen));

                    LevelRectangle rc = (LevelRectangle) view.findViewById(R.id.rec1);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    break;
                }
                case 2:
                {
                    tvScoreInPercent.setText(((score*100)/14) + "%");

                    tvCategoryDescription.setText("In dieser Kategorie werden dir etwas 'knifflige' Fragen zum jeweiligen Thema gestellt.");

                    //view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorCatOrange));

                    LevelRectangle rc = (LevelRectangle) view.findViewById(R.id.rec1);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    rc = (LevelRectangle) view.findViewById(R.id.rec2);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    break;

                }
                case 3:
                {
                    tvScoreInPercent.setText(((score*100)/21) + "%");

                    tvCategoryDescription.setText("In dieser Kategorie werden dir schwierige Fragen zum jeweiligen Thema gestellt.");

                    //view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorCatRedlight));

                    LevelRectangle rc = (LevelRectangle) view.findViewById(R.id.rec1);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    rc = (LevelRectangle) view.findViewById(R.id.rec2);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    rc = (LevelRectangle) view.findViewById(R.id.rec3);
                    rc.setBackgroundColor(Color.parseColor("#ffffff"));
                    break;
                }
            }

            return view;
        }

        private View initResultView(View view)
        {
            //Game Finished
            if(gameFinished())
            {
                view = mInflater.inflate(R.layout.result_categoryfinished_row, null);

                //init back to dashboard button
                btnEndGame = (Button) view.findViewById(R.id.btnDashboard);
                btnEndGame.setOnClickListener(Results.this);
            }
            else
            {
                //lvl Finished?
                if(levelCompleted())
                {
                    view = mInflater.inflate(R.layout.result_nextlvl_row, null);

                    //init back to dashboard button
                    btnEndGame = (Button) view.findViewById(R.id.btnDashboard);
                    btnEndGame.setOnClickListener(Results.this);
                }
                //lvl failed
                else
                {
                    view = mInflater.inflate(R.layout.result_lvlfailed_row, null);

                    TextView tvTopicsToLearn = (TextView) view.findViewById(R.id.id_tvTopicsToLearn);
                    String temp = "";
                    for(int i = 0; i< Question.topicsToLearn.size(); i++)
                    {
                        temp = temp + Question.topicsToLearn.get(i) + "\n";
                    }
                    System.out.println("TopicsToLearn: " + temp);

                    tvTopicsToLearn.setText(temp);

                    //init back to dashboard button
                    btnEndGame = (Button) view.findViewById(R.id.btnDashboard);
                    btnEndGame.setOnClickListener(Results.this);
                }
            }
            return view;
        }
    }

    private boolean gameFinished()
    {
        QuizDashboard.calculateOverallScore();

        if(QuizDashboard.gameFinished)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean levelCompleted()
    {
        if(score==(7*difficulty))
        {
            return true;
        }else
        {
            return false;
        }
    }
}
