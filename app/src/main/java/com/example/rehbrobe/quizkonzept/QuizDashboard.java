package com.example.rehbrobe.quizkonzept;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rehbrobe.impressum.ImpressumMainActivity;
import com.example.rehbrobe.knowledgebase.KnowledgebaseMainActivity;
import com.example.rehbrobe.quizlayout.Circle;
import com.example.rehbrobe.quizlayout.CircleAngleAnimation;
import com.example.rehbrobe.quizlayout.LevelRectangle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class QuizDashboard extends AppCompatActivity implements View.OnClickListener
{
    ListView categoryList;

    TextView tvCorrectQuSum, tvIncorrectQuSum, tvOverallScoreInPercent;

    Button btReset;

    Circle circle2;
    CircleAngleAnimation animation2;

    static JSONArray myJsonArray;

    final int maxScorePerTopic = 42;

    int overallScoreInAngle;

    static int maxScore, numberOfTopics=0, overallScore=0;

    double overallScoreInPercent;

    static boolean gameFinished = false;

    static ArrayList<String> hsKeysforTopics = new ArrayList<>();

    static SharedPreferences mySPR;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.quzi_dashboard);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        Menu menu =  navigation.getMenu();
        MenuItem item = menu.getItem(0);
        item.setChecked(true);

        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
        {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.navigationBar_LeftItem:
                    {
                        //Intent showDashboard = new Intent(QuizDashboard.this, QuizDashboard.class);
                        //startActivity(showDashboard);
                        break;
                    }


                    case R.id.navigationBar_CenterItem:
                    {
                        Intent showKB = new Intent(QuizDashboard.this, KnowledgebaseMainActivity.class);
                        startActivity(showKB);
                        break;
                    }

                    case R.id.navigationBar_RightItem:
                    {
                        Intent showAbout = new Intent(QuizDashboard.this, ImpressumMainActivity.class);
                        startActivity(showAbout);
                        break;
                    }
                }
                return false;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        try
        {
            JSONObject myJsonObj = new JSONObject(loadJSONFromAsset());

            myJsonArray = myJsonObj.getJSONArray("topics");

            System.out.println("JSON: " + myJsonArray.toString());

            System.out.println("Länge: " + myJsonArray.length());
            numberOfTopics = myJsonArray.length();
            maxScore = maxScorePerTopic * numberOfTopics;

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        mySPR = getSharedPreferences("mySPfile",MODE_PRIVATE);

        tvCorrectQuSum = (TextView) findViewById(R.id.id_questionNr);
        tvOverallScoreInPercent = (TextView) findViewById(R.id.id_question_score);
        tvIncorrectQuSum = (TextView) findViewById(R.id.id_questions_wrong);

        btReset = (Button) findViewById(R.id.id_btn_reset);
        btReset.setOnClickListener(this);

        categoryList = (ListView) findViewById(R.id.categoriesList);

        refreshDashboard();
    }

    public static void calculateOverallScore()
    {
        overallScore = 0;

        for (int i =0; i < myJsonArray.length(); i++)
        {
            overallScore = overallScore + sumUpScoreFromTopic(i);
        }

        if(overallScore==maxScore)
        {
            gameFinished=true;
        }
    }

    private static int sumUpScoreFromTopic(int topicNr)
    {
        int topicScore = 0;
        String topicName = null;

        try
        {
            JSONObject topic = myJsonArray.getJSONObject(topicNr);
            topicName = topic.getString("topicName");

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        for(int j=1; j <=3; j++)
        {
            String hsKeyOfSubTopic = topicName + "hs" + j;
            System.out.println(hsKeyOfSubTopic);

            hsKeysforTopics.add(hsKeyOfSubTopic);

            topicScore = topicScore + (mySPR.getInt(hsKeyOfSubTopic, 0));
            System.out.println("topicScore:" + topicScore);
        }
        return topicScore;
    }

    private void refreshDashboard()
    {
        tvCorrectQuSum.setText(String.valueOf(mySPR.getInt("correctQuSum",0)));
        tvIncorrectQuSum.setText(String.valueOf(mySPR.getInt("incorrectQuSum",0)));

        calculateOverallScore();
        System.out.println("OverallScore: " + overallScore);

        overallScoreInPercent =  (overallScore * 100.0) / maxScore;

        overallScoreInAngle = (overallScore * 360) / maxScore;

        circle2 = (Circle) findViewById(R.id.circle);
        circle2.setStrokeWidth(30);
        animation2 = new CircleAngleAnimation(circle2, overallScoreInAngle);
        animation2.setDuration(1500);
        animateTextView(0,(int)overallScoreInPercent,tvOverallScoreInPercent);
        circle2.startAnimation(animation2);

        categoryList.setAdapter(new CategoryListAdapter());
    }

    private boolean resetAllScores()
    {
        try
        {
            SharedPreferences.Editor mySPREditor = mySPR.edit();
            mySPREditor.clear();
            mySPREditor.apply();
            gameFinished=false;
            return true;
        }
        catch (Exception ex)
        {
            System.out.println("Error while clearing Preference File: " + ex.getMessage());
            return false;
        }
    }

    private String loadJSONFromAsset()
    {
        String json;

        try
        {
            InputStream is = getAssets().open("questions.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview)
    {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue().toString()+"%");

            }
        });
        valueAnimator.start();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.id_btn_reset :
            {
                System.out.println("Reset-Button pressed!");
                if(resetAllScores())
                {
                    refreshDashboard();
                    System.out.println("All scores have been reseted!");
                }
                else
                {
                    System.out.println("Error while resetting scores!");
                }
                break;
            }
        }
    }

    private class CategoryListAdapter extends BaseAdapter implements View.OnClickListener
    {

        private LayoutInflater mInflater;

        Button btPlay;

        TextView tvCategoryName, tvCategoryDescription, tvCategoryScore, tvCategoryScoreInPercent;

        public CategoryListAdapter()
        {
            mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount()
        {
            return ((myJsonArray.length())*3);
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            int temp;

            convertView = mInflater.inflate(R.layout.category_row, null);

            System.out.println("Position: " + position);

            tvCategoryName = (TextView) convertView.findViewById(R.id.id_cat_name);
            tvCategoryDescription = (TextView) convertView.findViewById(R.id.id_cat_desc);
            tvCategoryScore = (TextView) convertView.findViewById(R.id.id_cat_scorePoints);
            tvCategoryScoreInPercent = (TextView) convertView.findViewById(R.id.id_cat_scorePercentage);

            btPlay = (Button) convertView.findViewById(R.id.id_btn_play);
            btPlay.setVisibility(View.INVISIBLE);
            btPlay.setEnabled(false);

            String category ="Null";
            try
            {
                category = myJsonArray.getJSONObject((int)(position/3)).getString("topicName");

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            tvCategoryName.setText(category);


            //Fragen vom Schwierigkeitsgrad 1 (leicht)
            if(((position+1)%3)==1)
            {
                convertView.setBackgroundColor(ContextCompat.getColor(convertView.getContext(), R.color.colorCatGreen));

                convertView.findViewById(R.id.id_catRowBackG).setBackgroundColor(Color.TRANSPARENT);

                tvCategoryDescription.setText("In dieser Kategorie werden dir einfache Fragen zum jeweiligen Thema gestellt.");

                LevelRectangle rc = (LevelRectangle) convertView.findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));

                temp = mySPR.getInt(hsKeysforTopics.get(position),0);
                tvCategoryScore.setText(String.valueOf(temp));
                tvCategoryScoreInPercent.setText(String.valueOf((temp*100)/7) + " %");

                if(!gameFinished)
                {
                    btPlay.setVisibility(View.VISIBLE);
                    btPlay.setOnClickListener(this);
                    btPlay.setEnabled(true);
                }
                else
                {
                    btPlay.setVisibility(View.VISIBLE);
                    btPlay.setBackgroundResource(R.drawable.ic_action_completed);
                }
            }

            //Fragen vom Schwierigkeitsgrad 2 (mittel)
            if(((position+1)%3)==2)
            {
                convertView.setBackgroundColor(ContextCompat.getColor(convertView.getContext(), R.color.colorCatOrange));

                tvCategoryDescription.setText("In dieser Kategorie werden dir etwas 'knifflige' Fragen zum jeweiligen Thema gestellt.");

                LevelRectangle rc = (LevelRectangle) convertView.findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) convertView.findViewById(R.id.rec2);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));

                temp = mySPR.getInt(hsKeysforTopics.get(position),0);
                tvCategoryScore.setText(String.valueOf(temp));
                tvCategoryScoreInPercent.setText(String.valueOf((temp*100)/14) + " %");

                if(!gameFinished)
                {
                    if (mySPR.getInt(category + "hs1", 0) >= 7) {
                        convertView.findViewById(R.id.id_catRowBackG).setBackgroundColor(Color.TRANSPARENT);
                        btPlay.setVisibility(View.VISIBLE);
                        btPlay.setOnClickListener(this);
                        btPlay.setEnabled(true);
                    }
                }
                else
                {
                    btPlay.setVisibility(View.VISIBLE);
                    btPlay.setBackgroundResource(R.drawable.ic_action_completed);
                    //convertView.findViewById(R.id.id_catRowBackG).setBackgroundColor(Color.TRANSPARENT);
                }
            }

            //Fragen vom Schwierigkeitsgrad 3 (schwer)
            if(((position+1)%3)==0)
            {
                convertView.setBackgroundColor(ContextCompat.getColor(convertView.getContext(), R.color.colorCatRedlight));

                tvCategoryDescription.setText("In dieser Kategorie werden dir schwierige Fragen zum jeweiligen Thema gestellt.");

                LevelRectangle rc = (LevelRectangle) convertView.findViewById(R.id.rec1);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) convertView.findViewById(R.id.rec2);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));
                rc = (LevelRectangle) convertView.findViewById(R.id.rec3);
                rc.setBackgroundColor(Color.parseColor("#ffffff"));

                temp = mySPR.getInt(hsKeysforTopics.get(position),0);
                tvCategoryScore.setText(String.valueOf(temp));
                tvCategoryScoreInPercent.setText(String.valueOf((temp*100)/21) + " %");

                if(!gameFinished)
                {
                    if (mySPR.getInt(category + "hs2", 0) >= 14) {
                        convertView.findViewById(R.id.id_catRowBackG).setBackgroundColor(Color.TRANSPARENT);
                        btPlay.setVisibility(View.VISIBLE);
                        btPlay.setOnClickListener(this);
                        btPlay.setEnabled(true);
                    }
                }
                else
                {
                    btPlay.setVisibility(View.VISIBLE);
                    btPlay.setBackgroundResource(R.drawable.ic_action_completed);
                }
            }
            return convertView;
        }

        @Override
        public void onClick(View v)
        {
            View parentRow = (View) v.getParent();
            LinearLayout lltmp = (LinearLayout) parentRow.getParent();
            ConstraintLayout cltmp = (ConstraintLayout) lltmp.getParent();
            ListView listView = (ListView)cltmp.getParent();

            int position = listView.getPositionForView(parentRow);
            String category ="Null";
            try
            {
                category = myJsonArray.getJSONObject((int)(position/3)).getString("topicName");

            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            Intent startQuiz = new Intent(QuizDashboard.this, Question.class);
            startQuiz.putExtra("selectedRow",position);
            startQuiz.putExtra("selectedCategoryName", category);
            startActivity(startQuiz);
        }
    }
}
