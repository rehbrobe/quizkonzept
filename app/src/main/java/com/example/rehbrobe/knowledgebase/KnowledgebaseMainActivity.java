package com.example.rehbrobe.knowledgebase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.rehbrobe.adapter.KnowledgebaseSearchAdapter;
import com.example.rehbrobe.adapter.KnowledgebaseTopicAdapter;
import com.example.rehbrobe.impressum.ImpressumMainActivity;
import com.example.rehbrobe.quizkonzept.QuizDashboard;
import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by reitech on 11.01.18.
 */

public class KnowledgebaseMainActivity extends Activity {

    GridView topicGrid;
    SearchView searchView;

    JSONArray topicJSONArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.knowledgebase_overview);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        Menu menu =  navigation.getMenu();
        MenuItem item = menu.getItem(1);
        item.setChecked(true);

        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener()
        {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.navigationBar_LeftItem:
                    {
                        Intent showDashboard = new Intent(KnowledgebaseMainActivity.this, QuizDashboard.class);
                        startActivity(showDashboard);
                        break;
                    }


                    case R.id.navigationBar_CenterItem:
                    {
                        //Intent showKB = new Intent(KnowledgebaseMainActivity.this, KnowledgebaseMainActivity.class);
                        //startActivity(showKB);
                        break;
                    }

                    case R.id.navigationBar_RightItem:
                    {
                        Intent showAbout = new Intent(KnowledgebaseMainActivity.this, ImpressumMainActivity.class);
                        startActivity(showAbout);
                        break;
                    }
                }
                return false;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        
        //load topics from JSON
        loadKnowledgeBaseTopics();

        topicGrid = (GridView) findViewById(R.id.topic_grid);
        searchView = (SearchView) findViewById(R.id.search_view);

        final KnowledgebaseTopicAdapter topicAdapter = new KnowledgebaseTopicAdapter(this, topicJSONArray);
        topicGrid.setAdapter(topicAdapter);

        final KnowledgebaseSearchAdapter searchAdapter = new KnowledgebaseSearchAdapter(this, topicJSONArray);



        //set search text listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                topicGrid.setAdapter(searchAdapter);
                topicGrid.setNumColumns(1);
                searchAdapter.updateGridView(topicJSONArray);
                searchAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }

        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                topicGrid.setAdapter(topicAdapter);
                topicGrid.setNumColumns(2);
                topicAdapter.updateGridView(topicJSONArray);
                return false;
            }
        });

        //set grid click listener
        topicGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView textView = (TextView) view
                        .findViewById(R.id.topic_label);

                String topicName = textView.getText().toString();
                Intent showTopicIntent = new Intent(getApplicationContext(), ShowTopic.class);

                try{

                    for (int j = 0; j < topicJSONArray.length(); j++) {

                        JSONObject topic = topicJSONArray.getJSONObject(j);

                        if(topic.getString("heading").equals(topicName)){

                            showTopicIntent.putExtra("topic",topic.toString());
                            showTopicIntent.putExtra("selected",topicName);
                            showTopicIntent.putExtra("content", topic.getString("content"));
                            break;
                        }

                        //check subtopics too
                        JSONArray subTopics = topic.getJSONArray("subheadings");

                        for(int a = 0; a < subTopics.length(); a++){

                            JSONObject subTopic = subTopics.getJSONObject(a);

                            if(subTopic.getString("heading").equals(topicName)){

                                showTopicIntent.putExtra("topic",topic.toString());
                                showTopicIntent.putExtra("selected",topicName);
                                showTopicIntent.putExtra("content", subTopic.getString("content"));
                                break;
                            }
                        }
                    }

                    startActivity(showTopicIntent);
                }
                catch (JSONException e){

                }
            }
        });

    }

    private void loadKnowledgeBaseTopics() {


        try
        {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            topicJSONArray = obj.getJSONArray("topics");

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset() {

        String json;

        try
        {
            InputStream is = getAssets().open("knowledgebase.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
