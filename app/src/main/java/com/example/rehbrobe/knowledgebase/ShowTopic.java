package com.example.rehbrobe.knowledgebase;

import android.app.Activity;
import android.content.res.Resources;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rehbrobe.quizkonzept.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by reitech on 15.01.18.
 */

public class ShowTopic extends Activity {

    WebView txtView;
    ImageView imgView;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.knowledgebase_topic);

        txtView = (WebView) findViewById(R.id.topicContent);
        WebSettings w = txtView.getSettings();
        w.setPluginState(WebSettings.PluginState.ON);
        w.setJavaScriptEnabled(true);
        imgView = (ImageView) findViewById(R.id.topicimage);

        try {
            Bundle extras = getIntent().getExtras();
            JSONObject chosenTopic = new JSONObject(extras.getString("topic"));

            //extract content and subheadings
            String selectedHeading = extras.getString("selected");
            String mainText = extras.getString("content");
            String topicHeading = chosenTopic.getString("heading");
            JSONArray subTopics = chosenTopic.getJSONArray("subheadings");
            String imageID = chosenTopic.getString("image");

            String topicContent = "";
            if(topicHeading.equals(selectedHeading)){

                topicContent = generateHTMLTextofTopic(chosenTopic,selectedHeading,mainText,subTopics,true);

            }else{
                topicContent = generateHTMLTextofTopic(chosenTopic,selectedHeading,mainText,subTopics,false);
            }

            imgView.setImageResource(this.getResources().getIdentifier(imageID, "drawable", this.getPackageName()));
            txtView.loadDataWithBaseURL("app:htmlPage24",topicContent,"text/html","UTF-8", null);
        }
        catch (JSONException e){

            System.out.println(e.getStackTrace());
        }




    }

    private String generateHTMLTextofTopic(JSONObject chosenTopic, String heading, String mainText, JSONArray subTopics, boolean isChosenTopicMainTopic) {

        try {

        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body style=\"padding: 20px;\">\n";

        html = html + "<h2 style=\"color: #e2494c;\" id=\"topic\">" + heading + "</h2>";

        //check if subheading includes tutorial
        if(heading.contains("Wie verwendet man Kondome richtig?")){
            System.out.println("*******TUTORIAL*******");
            for (int i = 0; i < subTopics.length(); i++) {
                JSONObject subTopic = subTopics.getJSONObject(i);
                if(subTopic.getString("heading").contains("Wie verwendet man Kondome richtig?")){

                    //create tutorial for condome
                    html = html + "<p>" + createTutorialForCondome(subTopic, mainText) + "</p><br/><br/>";
                }
            }

        }else {
            html = html + "<p>" + mainText + "</p><br/><br/>";
        }

        html = html + "<h3 style=\"color: #e2494c;\" id=\"overview\">weitere Themen</h3>";

        //<a href="#top">Go to top</a>


            if(!isChosenTopicMainTopic){

                html = html + "<a href=\"#topicMain\">Einleitung: "+chosenTopic.getString("heading")+"</a><br/><br/>";
            }

            //attach anchors for subtopics
            for (int i = 0; i < subTopics.length(); i++) {
                JSONObject subTopic = subTopics.getJSONObject(i);


                html = html + "<a href=\"#topic"+i+"\">"+subTopic.getString("heading")+"</a><br/><br/>";


            }
            html = html + "<br/><br/><br/>";

            if(!isChosenTopicMainTopic){

                html = html + "<h2 style=\"color: #e2494c;\" id=\"topicMain\">Einleitung: " + chosenTopic.getString("heading") + "</h2>";
                html = html + "<p>" + chosenTopic.getString("content") + "</p> <a href=\"#overview\">zurück</a><br/><br/>";
            }

            //attach subheadings
            for (int i = 0; i < subTopics.length(); i++) {
                JSONObject subTopic = subTopics.getJSONObject(i);

                String subContentHeading = subTopic.getString("heading");
                html = html + "<h2 style=\"color: #e2494c;\" id=\"topic"+i+"\">" + subContentHeading + "</h2>";

                //check if subtopic includes tutorial
                if(subContentHeading.contains("Wie verwendet man Kondome richtig?")){
                    System.out.println("*******TUTORIAL*******");
                    //create tutorial content
                    html = html + "<p>" + createTutorialForCondome(subTopic,subTopic.getString("content")) + "</p> <a href=\"#overview\">zurück</a><br/><br/>";
                }else {
                    html = html + "<p>" + subTopic.getString("content") + "</p> <a href=\"#overview\">zurück</a><br/><br/>";
                }

            }

            return html+"</body>\n" +
                    "</html>";
        }
        catch (JSONException e){
            System.out.println(e.getStackTrace());

            return "</body>\n" +
                    "</html>";
        }


    }

    private String createTutorialForCondome(JSONObject chosenTopic, String mainText){

        String tutorialContent = "";
        try {
            JSONArray images = chosenTopic.getJSONArray("images");

            //attach images and captions
            for (int i = 0; i < images.length(); i++) {

                JSONObject imageObject = images.getJSONObject(i);
                String imageID = imageObject.getString("imageID");
                String caption = imageObject.getString("caption");
                tutorialContent = tutorialContent + "<img src=\"file:///android_res/drawable/"+imageID+"\" width=\"300px\" /><br><br>";
                tutorialContent = tutorialContent + caption + "<br><br><br><br>";

            }

            //attach main content
            tutorialContent = tutorialContent + mainText;


        }
        catch(JSONException e){
            e.printStackTrace();
        }

        return tutorialContent;
    }
}
